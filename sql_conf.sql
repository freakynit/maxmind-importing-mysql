CREATE TABLE `geo_103` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `beginIp` varchar(20) DEFAULT NULL,
  `endIp` varchar(20) DEFAULT NULL,
  `netMask` int(11) DEFAULT NULL,
  `beginIpNum` bigint(20) unsigned DEFAULT NULL,
  `endIpNum` bigint(20) unsigned DEFAULT NULL,
  `countryCode` varchar(255) DEFAULT NULL,
  `countryName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `geo_108` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `beginIp` varchar(20) DEFAULT NULL,
  `endIp` varchar(20) DEFAULT NULL,
  `beginIpNum` bigint(20) unsigned DEFAULT NULL,
  `endIpNum` bigint(20) unsigned DEFAULT NULL,
  `countryCode` varchar(255) DEFAULT NULL,
  `countryName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `geo_134_blocks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `startIpNum` bigint(20) unsigned DEFAULT NULL,
  `endIpNum` bigint(20) unsigned DEFAULT NULL,
  `locId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `geo_134_locations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `locId` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postalCode` int(11) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `metroCode` int(11) DEFAULT NULL,
  `areaCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `geo_139` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `startIpNum` bigint(20) unsigned DEFAULT NULL,
  `endIpNum` bigint(20) unsigned DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postalCode` int(11) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `dmaCode` int(11) DEFAULT NULL,
  `areaCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


